﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Threading;
using System.Windows.Forms;

namespace Similarity
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txtFilePath.Text = Properties.Settings.Default.filepath;
            txtSetting.Text = Properties.Settings.Default.setting;
        }
        OleDbConnection con;
        public bool isStop = false;
        public Dictionary<string, Dictionary<string, string>> dic = new Dictionary<string, Dictionary<string, string>>();


        /// <summary>
        /// Returns the number of steps required to transform the source string
        /// into the target string.
        /// </summary>
        int ComputeLevenshteinDistance(string source, string target)
        {
            if ((source == null) || (target == null)) return 0;
            if ((source.Length == 0) || (target.Length == 0)) return 0;
            if (source == target) return source.Length;

            int sourceWordCount = source.Length;
            int targetWordCount = target.Length;

            // Step 1
            if (sourceWordCount == 0)
                return targetWordCount;

            if (targetWordCount == 0)
                return sourceWordCount;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            // Step 2
            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    // Step 3
                    int cost = (target[j - 1] == source[i - 1]) ? 0 : 1;

                    // Step 4
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }

            return distance[sourceWordCount, targetWordCount];
        }

        /// <summary>
        /// Calculate percentage similarity of two strings
        /// <param name="source">Source String to Compare with</param>
        /// <param name="target">Targeted String to Compare</param>
        /// <returns>Return Similarity between two strings from 0 to 1.0</returns>
        /// </summary>
        double CalculateSimilarity(string source, string target)
        {
            if ((source == null) || (target == null)) return 0.0;
            if ((source.Length == 0) || (target.Length == 0)) return 0.0;
            if (source == target) return 1.0;

            int stepsToSame = ComputeLevenshteinDistance(source, target);
            return (1.0 - ((double)stepsToSame / (double)Math.Max(source.Length, target.Length)));
        }


        public void connectAccess(string path = "Database2.accdb")
        {
            try
            {
                con = new OleDbConnection($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path}");
                con.Open();
                MessageBox.Show("done");
                save();

            }
            catch
            {
                MessageBox.Show("Khhong the ket noi Access");
            }
        }

        public void test()
        {
            try
            {
                Thread main_thread = new Thread(() =>
                {
                    if (dic.Count == 0)
                    {
                        MessageBox.Show("Chua lay data lib");
                        return;
                    }

                    string setting = txtSetting.Text.Replace("\r", "");
                    string[] string_lib = setting.Split('\n');

                    string sql = $"SELECT COUNT(ID) FROM raw_data ";
                    OleDbCommand cmd = new OleDbCommand(sql, con);
                    OleDbDataReader raw_data = cmd.ExecuteReader();
                    raw_data.Read();
                    string count = raw_data[0].ToString();
                    progressBar1.Invoke(new Action(() => { progressBar1.Maximum = Convert.ToInt32(count); }));

                    sql = $"SELECT * FROM raw_data ORDER BY ID ASC";
                    cmd = new OleDbCommand(sql, con);
                    raw_data = cmd.ExecuteReader();

                    int index = 0;
                    while (raw_data.Read())
                    {
                        if (isStop)
                            break;
                        progressBar1.Invoke(new Action(() => { progressBar1.Value = ++index; }));

                        if (raw_data["hasCheck"].ToString() == "true")
                            continue;
                        string id = (raw_data["ID"].ToString());

                        List<Thread> listThead = new List<Thread>();
                        Dictionary<string, string> valueReturn = new Dictionary<string, string>();

                        foreach (string s in string_lib)
                        {
                            string[] setting_child = s.Split('|');

                            Thread t = new Thread(() =>
                            {
                                string text = raw_data[setting_child[1].ToString().Trim()].ToString();
                                foreach (var item in dic[setting_child[5].Trim()])
                                {
                                    double similarity = CalculateSimilarity(text, item.Key);
                                    if (similarity > Convert.ToDouble(setting_child[6].Trim()))
                                    {
                                        string chungloai_clean = item.Value;
                                        Console.WriteLine(similarity + " - " + chungloai_clean);
                                        valueReturn.Add(setting_child[2].Trim(), similarity.ToString());
                                        valueReturn.Add(setting_child[3].Trim(), item.Key.Replace("\"", ""));
                                        valueReturn.Add(setting_child[4].Trim(), item.Value);
                                        break;
                                    }
                                }
                            });
                            t.IsBackground = true;
                            t.Start();
                            listThead.Add(t);
                        }

                        foreach (var t in listThead) t.Join();
                        foreach (var t in listThead) t.Abort();

                        string textUpdate = null;
                        foreach (var i in valueReturn)
                        {
                            textUpdate += $"[{i.Key}] = \"{i.Value}\" ,";
                        }

                        sql = $"UPDATE raw_data SET {textUpdate}  hasCheck = \"true\" WHERE ID = \"{id}\" ";
                        OleDbCommand update = new OleDbCommand(sql, con);
                        update.ExecuteNonQuery();

                    }
                    raw_data.Close();
                    MessageBox.Show("Done");

                });
                main_thread.Start();
                main_thread.IsBackground = true;

            }
            catch (Exception e)
            {
                Console.WriteLine("looi -" + e.Message);
            }


        }


        private void getDataLib(string nameDic)
        {
            OleDbCommand cma_lib = con.CreateCommand();
            cma_lib.CommandText = $"SELECT * FROM {nameDic}";
            cma_lib.Connection = con;
            OleDbDataReader lib = cma_lib.ExecuteReader();
            Dictionary<string, string> lib_child = new Dictionary<string, string>();

            while (lib.Read())
            {
                try
                {
                    lib_child.Add(lib[0].ToString(), lib[1].ToString());
                }
                catch { }
            }
            dic.Add(nameDic, lib_child);
            MessageBox.Show("done - " + nameDic);
            Console.WriteLine(dic.Count);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            isStop = false;
            test();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string setting = txtSetting.Text.Replace("\r", "");
            string[] lib = setting.Split('\n');
            foreach (string s in lib)
            {
                string lib_child = s.Split('|')[5].ToString().Trim();
                Thread t = new Thread(() =>
                {
                    getDataLib(lib_child);
                });
                t.IsBackground = true;
                t.Start();
            }
            save();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            connectAccess(txtFilePath.Text);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            isStop = true;
        }

        public void save()
        {
            Properties.Settings.Default.filepath = txtFilePath.Text;
            Properties.Settings.Default.setting = txtSetting.Text;
            Properties.Settings.Default.Save();

        }
    }
}
